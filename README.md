# <Название сервиса>

## Резюме

Название: <Название сервиса>  
Домен: <Название домена>  
Назначение: <Описание назначения сервиса>  

## Разработка сервиса

Инструкцию описывающую разворот, запуск и тестирование сервиса на локальной машине можно найти в отдельном документе в [Confluence](https://greensight.atlassian.net/wiki/spaces/ENSI/pages/362676232/Backend-)

Регламент работы над задачами тоже находится в [Confluence](https://greensight.atlassian.net/wiki/spaces/ENSI/pages/477528081)

## Структура сервиса

Почитать про структуру сервиса можно здесь [здесь](docs/structure.md)

## Зависимости

|Название|Описание|Переменные окружения|
|---|---|---|
|PostgreSQL|Основная БД сервиса|DB_*|
|   |   |   |
|   |   |   |

## Среды

### Test

CI: https://jenkins-infra.ensi.tech/job/ensi-stage-1/job/some-domain-name/job/some-service-name/  
URL: https://some-domain-here/docs/swagger  

### Preprod

Отсутствует

### Prod

Отсутствует

## Контакты

Команда поддерживающая данный сервис: https://gitlab.com/groups/greensight/ensi/-/group_members  
Email для связи: mail@greensight.ru

## Лицензия

[Открытая лицензия на право использования программы для ЭВМ Greensight Ecom Platform (GEP)](LICENSE.md).

